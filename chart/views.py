import base64
import io
import urllib

import matplotlib.pyplot as plt
import numpy as np
from django.shortcuts import render


def home(request):
    t = np.arange(0.0, 2.0, 0.01)
    s1 = np.sin(2 * np.pi * t)
    s2 = np.sin(2 * np.pi * t + np.pi / 2)

    plt.plot(t, s1)
    plt.plot(t, s2, color='red')
    plt.grid(True)
    fig = plt.gcf()
    
    # Convert graph into dtriping buffer and then we convert 64 bit code into image
    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    buf.seek(0)
    string = base64.b64encode(buf.read())
    uri = urllib.parse.quote(string)
    
    return render(request, 'home.html', {'data': uri})
